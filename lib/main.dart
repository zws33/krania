import 'package:flutter/material.dart';
import 'package:krania/screens/home_screen.dart';

import 'constants/colors.dart' as colors;

void main() {
  runApp(MaterialApp(
      theme: ThemeData.dark().copyWith(
        primaryColor: colors.lightGreen,
        scaffoldBackgroundColor: colors.darkGreen,
      ),
      initialRoute: '/',
      routes: {
        HomeScreen.routeName : (context) => HomeScreen(),
      },
    ),
  );
}
