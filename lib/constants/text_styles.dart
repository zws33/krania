
import 'package:flutter/material.dart';

import 'colors.dart' as colors;

const labelTextStyle = TextStyle(
  fontSize: 18.0,
  color: colors.white,
  fontFamily: 'Roboto',
  fontWeight: FontWeight.w400
);

const numberTextStyle = TextStyle(
  fontSize: 36.0,
  fontWeight: FontWeight.w900,
  color: colors.white
);