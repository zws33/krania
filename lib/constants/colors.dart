import 'dart:ui';

const Color lightGreen = Color.fromRGBO(147, 177, 167, 1);
const Color lightGreen2 = Color.fromRGBO(107, 137, 127, 1);
const Color peach = Color.fromRGBO(205, 165, 164, 1);
const Color darkGreen = Color.fromRGBO(69, 96, 79, 1);
const Color darkGreen2 = Color.fromRGBO(89, 116, 99, 1);
const Color white = Color.fromRGBO(245, 245, 245, 1);
const Color grey = Color.fromRGBO(245, 245, 245, 0.45);
